package com.project.adapters_exercise;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText mName, mReg;
    private Spinner mProgram;
    private Button mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mName = (EditText) findViewById(R.id.editTextName);
        mReg = (EditText) findViewById(R.id.editTextRegNo);
        mProgram = (Spinner) findViewById(R.id.spinnerProgram);
        mRegister = (Button) findViewById(R.id.btnRegister);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, getResources()
                .getStringArray(R.array.program_array));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mProgram.setAdapter(adapter);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String name = mName.getText().toString();
                String program = mProgram.getSelectedItem().toString();

                if (!name.equals("")) {
                    Log.d("Adapters", "Name is: " + name);
                    Log.d("Adapters", "Selected program: " + program);
                } else {
                    Log.d("Adapters", "Please enter your name");
                }
            }
        });


    }
}